package chatstream.factory;

import java.util.function.Function;

import javax.inject.Singleton;

import org.reactivestreams.Publisher;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableOnSubscribe;

@Singleton
public class PublisherFactory {

	private Function<FlowableOnSubscribe<?>, Flowable<?>> ctor;
	
	public PublisherFactory() {
		ctor = subscriber ->  Flowable.create(subscriber, BackpressureStrategy.BUFFER);
	}
	
	// Visible for testing
	public PublisherFactory(Function<FlowableOnSubscribe<?>, Flowable<?>> flowableConstructor) {
		ctor = flowableConstructor;
	}
	
	@SuppressWarnings("unchecked")
	public <T> Publisher<T> create(FlowableOnSubscribe<T> emitter) {
		return (Publisher<T>) ctor.apply(emitter).serialize().publish().autoConnect();
	}
}
