package chatstream.source.chat;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.reactivestreams.Publisher;

import chatstream.controllers.chat.model.ChatMessage;
import chatstream.factory.PublisherFactory;
import io.micronaut.core.util.StringUtils;
import io.micronaut.http.sse.Event;
import io.reactivex.Emitter;

@Singleton
public class ChatStream {

	
	private final PublisherFactory publisherFactory;
	private Publisher<Event<ChatMessage>> source;
	private Emitter<Event<ChatMessage>> emitter;
	
	public ChatStream(PublisherFactory publisherFactory) {
		this.publisherFactory = publisherFactory;
		
	}

	@PostConstruct
	public void init() {
		source = publisherFactory.create(emitter -> this.emitter = emitter);
	}

	
	public void publish(ChatMessage message) {
		if (StringUtils.isEmpty(message.getUsername())) {
			message.setUsername("Anonymous user");
		}
		emitter.onNext(Event.of(message));
	}
	
	public Publisher<Event<ChatMessage>> getPublisher() {
		return source;
	}
}
