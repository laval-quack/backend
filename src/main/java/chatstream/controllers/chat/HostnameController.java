package chatstream.controllers.chat;

import java.net.UnknownHostException;

import chatstream.controllers.chat.model.InfoMessage;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller("/")
public class HostnameController {

    @Value("${my.property:'n/a'}")
    private String property;

    @Get
    @Produces(MediaType.TEXT_PLAIN)
    public String index() {

        return "Hello World";
    }

    @Get("/info")
    public InfoMessage getInfo()
        throws UnknownHostException {

        return InfoMessage.createInfo();
    }

    @Get("/property")
    @Produces(MediaType.TEXT_PLAIN)
    public String getProperty() {

        return property;
    }

}
