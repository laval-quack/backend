package chatstream.controllers.chat;

import org.reactivestreams.Publisher;

import chatstream.controllers.chat.model.ChatMessage;
import chatstream.source.chat.ChatStream;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.sse.Event;

@Controller("/chat")
public class ChatController {
	
	private final ChatStream chatStream;
	
	public ChatController(ChatStream chatStream) {
		this.chatStream = chatStream;
	}
	
    @Get("/")
    public Publisher<Event<ChatMessage>> listen() {
    	return chatStream.getPublisher();
    }
    
    @Post
    public void publishMessage(@Body ChatMessage message) {
    	chatStream.publish(message);
    }
}
