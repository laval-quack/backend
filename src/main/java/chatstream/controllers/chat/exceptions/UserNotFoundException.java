package chatstream.controllers.chat.exceptions;

/**
 * UserNotFoundException
 */
public class UserNotFoundException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 434851198638523267L;

    public UserNotFoundException(final String username){
        super("User " + username + " not Found");
    }

    
}