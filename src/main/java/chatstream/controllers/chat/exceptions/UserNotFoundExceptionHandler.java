package chatstream.controllers.chat.exceptions;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import javax.inject.Singleton;

import chatstream.controllers.chat.model.ErrorMessage;

@Produces
@Singleton 
@Requires(classes = {UserNotFoundException.class, ExceptionHandler.class})  
public class UserNotFoundExceptionHandler implements ExceptionHandler<UserNotFoundException, HttpResponse<ErrorMessage>> { 

    @Override
    public HttpResponse<ErrorMessage> handle(HttpRequest request, UserNotFoundException exception) {
        return HttpResponse.badRequest(new ErrorMessage("USER_NOT_FOUND", exception.getMessage())); 
    }
}