package chatstream.controllers.chat.model;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InfoMessage {

    private final String hostname;

    private final String ipAddr;

    private InfoMessage(
        final String hostname,
        final String ipAddr) {

        this.hostname = hostname;
        this.ipAddr = ipAddr;
    }

    public static InfoMessage createInfo()
        throws UnknownHostException {

        final InetAddress ip = InetAddress.getLocalHost();
        final String hostname = ip.getHostName();

        return new InfoMessage(hostname, ip.toString());
    }

    public String getHostname() {

        return hostname;
    }

    public String getIpAddr() {

        return ipAddr;
    }
}
