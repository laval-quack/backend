package chatstream.controllers.chat.model;


/**
 * ErrorMessage
 */
public class ErrorMessage {

    public final String code;
    public final String message;

    public ErrorMessage(final String code, final String message){
        this.code = code;
        this.message = message;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
}