package chatstream.controllers.chat.model;

import java.util.Set;

public class ListUserMessage {

    private  Set<String> users;

    public ListUserMessage(Set<String> users){
        this.users = users;
    }

    /**
     * @return the users
     */
    public Set<String> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(Set<String> users) {
        this.users = users;
    }
}