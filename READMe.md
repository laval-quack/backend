## API Structure

/chat => gere le chat
   GET  /chat => get messages per room
   POST /chat => post a message in a room
   GET /chat/users get list of users
   POST /chat/users add user to the chat
 